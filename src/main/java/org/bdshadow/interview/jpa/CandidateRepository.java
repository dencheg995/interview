package org.bdshadow.interview.jpa;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CandidateRepository extends CrudRepository<Candidate, Integer> {

    @Override
    List<Candidate> findAll();

    @Query("SELECT candidate FROM Candidate candidate WHERE candidate.status IS NULL")
    List<Candidate> findAllWithNoStatus();
}
